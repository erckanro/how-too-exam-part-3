import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/header/header";
import Homepage from "./components/pages/homepage";

function Home() {
  return (
    <div className="App">
      <header>
        <Header />
      </header>
      <main>
        <Homepage />
      </main>
    </div>
  );
}

export default Home;
