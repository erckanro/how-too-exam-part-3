import { BsHouseDoorFill } from "react-icons/bs";

const headerOptions = [
    { option : "Home", path: "/", icon: <BsHouseDoorFill/> },
    { option : "Topic 1", path: "/topic1" },
    { option : "Topic 2", path: "/topic2" },
    { option : "Topic 3", path: "/topic3" },
    { option : "Topic 4", path: "/topic4" },
    { option : "Topic 5", path: "/topic5" },
]

export default headerOptions;
