import {Row, Col, Image} from 'react-bootstrap';

export default function Landing () {
    return (
        <Row>
            <Col sm={3} className="bg-dblue h-vh">
            <div className="py-50">
              <div className="logo-lg p-30">
                <Image
                src="../images/logo.png"
                fluid
              />
              </div>
              <div className="txt-white txt-nunito p-30">
                <p>lOREM iPSUM</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae nisl elit</p>
                <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eu leo elit. Donec condimentum odio ut ante bibendum, quis egestas quam semper.</p>
                <p> Donec tempus cursus magna a feugiat. Morbi scelerisque euismod aliquet. Integer ut euismod risus. Aenean a diam vel nisl accumsan cursus.</p>
                <p> Quisque ac dapibus nisl. Etiam rhoncus aliquet ligula ac convallis. Ut eleifend sit amet justo eu laoreet.</p>
              </div>
            </div>
            </Col>
            <Col sm={9} className="pos-relative homepage no-overflow">
            <div className="pos-absolute top-0 lft-0 w-full">
              <Image
                src="../images/home1-bg.jpg"
                className="w-full"
              />
            </div>
              
            </Col>
          </Row>
    )
}