import {Container, Nav, Navbar} from 'react-bootstrap';
import { Link } from "react-router-dom";
import style from "./header.module.scss";
import headerOptions from "./header.map.js";

export default function Header () {
    
    const pathname = window.location.pathname;

    return (
        <Navbar fixed="bottom" className={pathname.includes("topic") ? `${style.navWrapperGradient}` : `${style.navWrapperFade}`}>
            <Container>
                <Nav className="me-auto d-flex justify-content-between w-full">
                    {
                        headerOptions.map( ({ option, path, icon }) => (
                            <Link 
                                key = { path }
                                className = {` ${path === pathname ? "active" : ""} ${style.link} txt-18 txt-montserrat txt-white p-10 txt-uc `}
                                to = { path }>
                                { icon } { option }
                            </Link>
                        ))
                    }
                </Nav>
            </Container>
        </Navbar>
    )
}