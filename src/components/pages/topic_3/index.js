import { Container } from 'react-bootstrap';
import Landing from "./components/landing";
import CTA from "./components/cta";

export default function Topic_3 () {
    return (
        <Container fluid>
            <Landing />
            <CTA />
        </Container>
    )
}