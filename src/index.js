import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import Home from './App';
import reportWebVitals from './reportWebVitals';
import Topic1 from "./topic1";
import Topic2 from "./topic2";
import Topic3 from "./topic3";
import { createBrowserRouter, RouterProvider, } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById('root'));

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/topic1",
    element: <Topic1 />,
  },
  {
    path: "/topic2",
    element: <Topic2 />,
  },
  {
    path: "/topic3",
    element: <Topic3 />,
  },
]);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
