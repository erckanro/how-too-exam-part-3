import { Row, Col, Accordion, Container } from 'react-bootstrap';
import style from "./components/topic1.module.scss";
import accordionTopics from "./components/accordion.map.js";

export default function Topic_2 () {
    return (
        <Container fluid>
            <Row className={`${style.background}`} style={{ backgroundImage: `url(../images/home1-bg.jpg)` }}>
                <Col sm={3} className="bg-fade-to-left h-vh">
                <div className="py-50 ">
                    <div className="txt-white p-30 ">
                        <p className=" txt-45 txt-montserrat txt-weight-900 txt-uc">Nulla Imperdiet</p>
                        <p className="txt-white txt-18 txt-nunito txt-weight-100">
                            Vestibulum dapibus hendrerit nibh ut ornare
                            <span className={`d-block mt-3 w-250 pl-15 ${style.highlight}`}>
                                Sed porta, lorem a sodales rhoncus, neque ligula dictum libero, sit amet.
                            </span>
                        </p>
                    </div>
                </div>
                </Col>
                <Col sm={9} className="pos-relative homepage no-overflow">
                    <div className="w-500 mt-50">
                        <Accordion defaultActiveKey="1"  className={style.header}>
                            <div className="text-center txt-montserrat txt-18 my-20">Montserrat</div>
                        {
                            accordionTopics.map( ({ id, title, description }) => (
                                <Accordion.Item eventKey = { id } key = { id }>
                                    <Accordion.Header> { title } </Accordion.Header>
                                    <Accordion.Body> { description } </Accordion.Body>
                                </Accordion.Item>
                            ))
                        }
                        </Accordion>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}