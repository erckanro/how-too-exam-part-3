import { Fragment } from "react";
import Header from "./components/header/header";
import Topic_2 from "./components/pages/topic_2";

import "bootstrap/dist/css/bootstrap.min.css";

function Topic2() {
  return (
    <div className="">
        <Fragment>
          <Header />
        </Fragment>
        <Topic_2 />
    </div>
  );
}

export default Topic2;