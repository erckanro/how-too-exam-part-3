const accordionTopics = [
    { 
        id: 1,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 2,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 3,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 4,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 5,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 6,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
    { 
        id: 7,
        title: "Lorem Ipsum",
        description: "Proin sed massa laoreet, viverra dolor vitae, sodales ipsum. Sed quis massa pulvinar justo interdum tempor. Maecenas lobortis tristique massa, sit amet malesuada elit dignissim ut."
    },
]

export default accordionTopics;
