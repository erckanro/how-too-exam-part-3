import { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/header/header";
import Topic_1 from "./components/pages/topic_1";

function Topic1() {
  return (
    <div className="">
        <Fragment>
          <Header />
        </Fragment>
        <Topic_1 />
    </div>
  );
}

export default Topic1;