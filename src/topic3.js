import { Fragment } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/header/header";
import Topic_3 from "./components/pages/topic_3";

function Topic3() {
  return (
    <div className="">
        <Fragment>
          <Header />
        </Fragment>
        <Topic_3 />
    </div>
  );
}

export default Topic3;