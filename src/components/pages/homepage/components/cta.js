import {Row, Col, Image, Button} from 'react-bootstrap';
import style from "./cta.module.scss";

export default function CTA () {
    return (
        <Row>
            <Col sm={3} className="bg-dblue h-vh">
            <div className="py-50 ">
                <div className="custom logo-sm ml-30">
                    <Image fluid src="../images/logo.png" />
                </div>
                <div className="txt-white txt-45 txt-montserrat txt-weight-900 p-30 ">
                <p className="mt-60 mb-45 txt-uc">lOREM iPSUM DOLOR SIT</p>
                <Button className={ style.cta }>
                    <span className="txt-16 txt-montserrat txt-black txt-uc">Begin</span>
                </Button>
                </div>
            </div>
            </Col>
            <Col sm={9} className="pos-relative homepage no-overflow">
            <div className="pos-absolute top-0 lft-0 w-full">
                <Image src="../images/home2-bg.jpg" />
            </div>
                
            </Col>
        </Row>
    )
}